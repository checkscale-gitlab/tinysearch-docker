# tinysearch-docker

A Docker image for [tinysearch](https://github.com/mre/tinysearch). a
lightweight, fast, full-text search engine for static websites. Usage:

```dockerfile
FROM registry.gitlab.com/aedge/tinysearch-docker AS search
COPY index.json /index.json
RUN tinysearch /index.json

FROM your-base-image
COPY --from=search
```

TODO add more examples